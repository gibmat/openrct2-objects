openrct2-objects (1.5.1+dfsg-1) unstable; urgency=medium

  * New upstream release

 -- Mathias Gibbens <gibmat@debian.org>  Sun, 02 Feb 2025 19:47:40 +0000

openrct2-objects (1.4.12+dfsg-1) unstable; urgency=medium

  * New upstream release
    - Rebase patch
  * Update years in d/copyright

 -- Mathias Gibbens <gibmat@debian.org>  Thu, 09 Jan 2025 01:41:42 +0000

openrct2-objects (1.4.11+dfsg-1) unstable; urgency=medium

  * New upstream release

 -- Mathias Gibbens <gibmat@debian.org>  Tue, 10 Dec 2024 01:24:15 +0000

openrct2-objects (1.4.10+dfsg-1) unstable; urgency=medium

  * New upstream release

 -- Mathias Gibbens <gibmat@debian.org>  Mon, 04 Nov 2024 00:07:06 +0000

openrct2-objects (1.4.8+dfsg-1) unstable; urgency=medium

  * New upstream release

 -- Mathias Gibbens <gibmat@debian.org>  Sun, 06 Oct 2024 18:08:49 +0000

openrct2-objects (1.4.7+dfsg-1) unstable; urgency=medium

  * New upstream release

 -- Mathias Gibbens <gibmat@debian.org>  Mon, 05 Aug 2024 03:08:42 +0000

openrct2-objects (1.4.6+dfsg-1) unstable; urgency=medium

  * New upstream release

 -- Mathias Gibbens <gibmat@debian.org>  Sun, 07 Jul 2024 17:01:22 +0000

openrct2-objects (1.4.4+dfsg-1) unstable; urgency=medium

  * New upstream release
  * Update Standards-Version to 4.7.0 in d/control (no changes needed)

 -- Mathias Gibbens <gibmat@debian.org>  Sun, 05 May 2024 14:38:46 +0000

openrct2-objects (1.4.3+dfsg-1) unstable; urgency=medium

  * New upstream release

 -- Mathias Gibbens <gibmat@debian.org>  Tue, 02 Apr 2024 22:02:35 +0000

openrct2-objects (1.4.0+dfsg-1) unstable; urgency=medium

  * New upstream release
    - Rebase patch
  * Update years in d/copyright

 -- Mathias Gibbens <gibmat@debian.org>  Sat, 02 Mar 2024 22:47:13 +0000

openrct2-objects (1.3.13+dfsg-1) unstable; urgency=medium

  * New upstream release

 -- Mathias Gibbens <gibmat@debian.org>  Sun, 31 Dec 2023 18:13:00 +0000

openrct2-objects (1.3.11+dfsg-1) unstable; urgency=medium

  * Upload to unstable

 -- Mathias Gibbens <gibmat@debian.org>  Sun, 11 Jun 2023 14:00:58 +0000

openrct2-objects (1.3.11+dfsg-1~exp1) experimental; urgency=medium

  * New upstream release

 -- Mathias Gibbens <gibmat@debian.org>  Mon, 08 May 2023 23:49:19 +0000

openrct2-objects (1.3.8+dfsg-1~exp1) experimental; urgency=medium

  * New upstream release
  * Update years in d/copyright
  * Bump Standards-Version in d/control

 -- Mathias Gibbens <gibmat@debian.org>  Tue, 28 Mar 2023 22:06:59 +0000

openrct2-objects (1.3.7+dfsg-1) unstable; urgency=medium

  * New upstream release
    - Update list of excluded files in d/copyright and refresh the
      corresponding patch
  * Update my email in d/control
  * Update default branch to debian/sid in d/gbp.conf

 -- Mathias Gibbens <gibmat@debian.org>  Thu, 15 Dec 2022 23:32:52 +0000

openrct2-objects (1.3.5+dfsg-1) unstable; urgency=medium

  * New upstream release
    - Update list of excluded files in d/copyright and refresh the
      corresponding patch
  * I change the licensing of my packaging work to match the upstream
    licensing (GPL-3 -> CC-BY-4.0)
  * Add d/salsa-ci.yml
    - Make builds reproducible
  * Fix d/watch after GitHub changes
  * Update d/source/lintian-overrides

 -- Mathias Gibbens <mathias@calenhad.com>  Wed, 05 Oct 2022 23:25:05 +0000

openrct2-objects (1.3.2+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Update Standards-Version to 4.6.1 (no changes needed)
  * Update list of excluded files in d/copyright

 -- Mathias Gibbens <mathias@calenhad.com>  Mon, 04 Jul 2022 19:32:39 +0000

openrct2-objects (1.2.7+dfsg-1) unstable; urgency=medium

  * New upstream version.
  * d/copyright:
    - Cleanup as releases now include license information
    - Update list of excluded files to reflect upstream renaming
    - Update years
  * Add d/gbp.conf
  * Bump Standards-Version to 4.6.0.1 (no changes needed).

 -- Mathias Gibbens <mathias@calenhad.com>  Mon, 25 Apr 2022 18:55:46 +0000

openrct2-objects (1.0.21+dfsg-3) unstable; urgency=medium

  * Remove a couple lingering references to non-dfsg objects.

 -- Mathias Gibbens <mathias@calenhad.com>  Mon, 19 Jul 2021 14:09:46 +0000

openrct2-objects (1.0.21+dfsg-2) unstable; urgency=medium

  * No-changes source-only upload.

 -- Mathias Gibbens <mathias@calenhad.com>  Sun, 18 Apr 2021 18:59:53 +0000

openrct2-objects (1.0.21+dfsg-1) unstable; urgency=medium

  * Initial release. (Closes: #985802)

 -- Mathias Gibbens <mathias@calenhad.com>  Wed, 24 Mar 2021 21:09:15 +0000
